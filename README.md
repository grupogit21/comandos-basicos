# comandos-basicos

## Titulo 2

### Titulo 3


Utilização dos **comandos** _básicos_ do git

* Lista 1
* Lista 2
* Lista 3

1. Lista 1
2. Lista 2
3. Lista 3

![Nome da imagem](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/768px-Git-logo.svg.png)

```
    <body>
        <h1> Hello Word!</h1>
        <p> Essa é minha primeira página na web </p>
        <p> Minha primeira alteracao na ramificacao </p>
    </body>
```